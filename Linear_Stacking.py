#!/usr/bin/env python
# coding: utf-8

# In[10]:


import pandas as pd
import numpy as np


# In[11]:


data_stack = pd.read_csv("Linear Stacking Coefficients.csv")
data_stack.replace("?", np.nan)

data_stack = data_stack.replace('N/A',float('NaN'))

num_cols = ['Model', 'KNN', 'Regression']


# In[15]:


def ls_compute(R, k, model):
    print(R,k,model)
    print(data_stack)
    if (model in data_stack['Model'].unique()):
       weight_k = data_stack[data_stack['Model']==model]['KNN'].iloc[0]
       weight_r = data_stack[data_stack['Model']==model]['Regression'].iloc[0]
    else:
       weight_k = 0.5
       weight_r = 0.5
    

    prediction = R*weight_r + k*weight_k

    return prediction


def ls_compute2(R, k, model):
    
    if (model in data_stack['Model'].unique()):
       weight_k = data_stack[data_stack['Model']==model]['KNN'].iloc[0]
       weight_r = data_stack[data_stack['Model']==model]['Regression'].iloc[0]
    else:
       weight_k = 0.5
       weight_r = 0.5

    if(weight_r < 0):
        prediction = k
    elif(weight_k < 0):
        prediction = r
    else:
        prediction = R*weight_r + k*weight_k

    return prediction

