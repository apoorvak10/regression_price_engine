import xlrd
import xlwt

#print("Let the Fun Begin...")
wb_in = xlrd.open_workbook(r'cars_gsheet.xlsx')

#disc
sheet_name = wb_in.sheet_names()[0]
ws_in2 = wb_in.sheet_by_name(sheet_name)

#prod
sheet_name = wb_in.sheet_names()[2]
ws_in3 = wb_in.sheet_by_name(sheet_name)

#listing table
# wb_in1 = xlrd.open_workbook(r'helper_data.xlsx')
# sheet_name = wb_in1.sheet_names()[0]
# ws_in1 = wb_in1.sheet_by_name(sheet_name)



wb_out = xlwt.Workbook()
ws_out1 = wb_out.add_sheet('Listing ORP City Wise',cell_overwrite_ok=True)


def get_orp(var_variant, var_bt, var_fuel, var_tran, var_year, city_id):

    row_out1 = 0
    bt = ["Hatchback","Sedan","MPV","SUV","Coupe","Convertible","Liftback","Crossover","Pickup","Targa"]
    fuels = ["Petrol","Diesel","CNG","LPG","Electric"]
    ABS = ["abs"]
    transmission = ["Manual","Automatic"]

    bt = [x.lower() for x in bt]
    fuels = [x.lower() for x in fuels]
    ABS = [x.lower() for x in ABS]
    transmission = [x.lower() for x in transmission]

    count = 0
    for row_in1 in range(1):
        #listings data
        is_entry_done = False
        row_out1+=1
        # if(row_out1%1000==0):
        #     print(row_out1)
        #     break
        flag = False
        #print(row_out1)
        min_d = 1e9
        min_d_mv = 1e9
        #row1 = ws_in1.row_values(row_in1)
        var_major = False

        # var_variant = input("Enter Variant Name : ")
        # var_bt = input("Enter BodyType : ")
        # var_fuel = input("Enter FuelType : ")
        # var_tran = input("Enter Transmission : ")
        variant = var_variant.lower().split(' ')
        # var_bt = ''
        # var_abs= ''
        # var_tran = ''
        # var_fuel = ''
        var = ''
        for i in range(len(variant)):
            if(variant[i] in bt):
                var_bt = variant[i]
            elif(variant[i] in fuels):
                var_fuel = variant[i]
            elif(variant[i] in ABS):
                var_abs = variant[i]
            elif(variant[i] in transmission):
                var_tran = variant[i]
            else:
                var+=(variant[i])
        #var_year = input("Enter Registration Year : ")
        sort_var = ''.join(sorted(list(str(var).replace(' ','').lower())))#.sort() #row1[0]
        reg_year = 1990 if (var_year)=='' else int(var_year)
        #print(sort_var)
        

        for row_in3 in range(1,ws_in3.nrows):
            #production exactt match
            row2 = ws_in3.row_values(row_in3)
            
            start_year = 0 #if (row2[6])=='' else int(row2[6])
            end_year = 1e9 #if (row2[7])=='' else int(row2[7])
            # if(reg_year<start_year):
            #     continue
            variant2 = row2[5].lower().split(' ')
            var2_bt = ''
            var2_abs= ''
            var2_tran = ''
            var2_fuel = ''
            var2 = ''

            if((row2[5]).lower()==(var_variant).lower()):
                flag = True
                #print(row_out1,"exact_match production")
                # ws_out1.write(row_out1,0,row1[0])
                # ws_out1.write(row_out1,1,row1[1])
                # ws_out1.write(row_out1,2,row1[2])
                # ws_out1.write(row_out1,3,row1[3])
                # ws_out1.write(row_out1,4,row1[4])
                # ws_out1.write(row_out1,5,row1[5])
                # ws_out1.write(row_out1,6,row1[6])
                # ws_out1.write(row_out1,7,row1[7])
                # ws_out1.write(row_out1,8,row1[8])
                # ws_out1.write(row_out1,9,row1[9])
                # ws_out1.write(row_out1,10,row1[10])
                # ws_out1.write(row_out1,11,row1[11])
                # ws_out1.write(row_out1,12,row1[12])
                # ws_out1.write(row_out1,13,row1[13])
                # ws_out1.write(row_out1,14,row1[14])
                # ws_out1.write(row_out1,15,row1[15])
                
                ws_out1.write(row_out1,18,row2[0])
                ws_out1.write(row_out1,19,row2[1])
                ws_out1.write(row_out1,20,row2[2])
                ws_out1.write(row_out1,21,row2[3])
                ws_out1.write(row_out1,22,row2[4])
                ws_out1.write(row_out1,23,row2[5])
                ws_out1.write(row_out1,24,row2[6])
                ws_out1.write(row_out1,25,row2[7])
                ws_out1.write(row_out1,26,row2[8])
                ws_out1.write(row_out1,27,row2[9])
                ws_out1.write(row_out1,28,row2[10])
                ws_out1.write(row_out1,29,row2[11])
                ws_out1.write(row_out1,30,row2[12])
                ws_out1.write(row_out1,31,row2[13])
                ws_out1.write(row_out1,32,row2[14])
                ws_out1.write(row_out1,33,row2[15])
                ws_out1.write(row_out1,34,row2[16])
                ws_out1.write(row_out1,35,row2[17])
                ws_out1.write(row_out1,36,row2[18])
                ws_out1.write(row_out1,37,row2[19])
                ws_out1.write(row_out1,38,row2[20])
                ws_out1.write(row_out1,39,row2[21])
                ws_out1.write(row_out1,40,"exact_match production")
                is_entry_done = True
                break

            if(variant[0] not in variant2 or variant[1] not in variant2):
                continue
            for i in range(len(variant2)):
                if(variant2[i] in bt):
                    var2_bt = variant2[i]
                elif(variant2[i] in fuels):
                    var2_fuel = variant2[i] 
                elif(variant2[i] in ABS):
                    var2_abs = variant2[i]
                elif(variant2[i] in transmission):
                    var2_tran = variant2[i]
                else:
                    var2+=(variant2[i])
                #cnt=0
                #    cnt+=1
                #min_d = min(cnt,min_d)
            sort2_var = ''.join(sorted(list(str(var2).replace(' ','').lower())))#.sort() #row1[0]

            if(sort_var==sort2_var):
                #fuel
                if not (str(row2[11]).lower()==str(var_fuel).lower()):# or str(row2[11]).lower()==var_fuel or var_fuel==var2_fuel):
                    continue
                #bt
                # if not (str(row2[12]).lower()==str(var_bt).lower()):# or str(row2[12]).lower()==var_bt or var_bt==var2_bt):
                #     continue
                #abs - Can't do as listings doesn't contain that
                #if not (str(row2[14]).lower()==str(row1[11]).lower() or str(row2[14]).lower()==var_fuel or var_fuel==var2_fuel):
                #    continue
                #tran
                if not (str(row2[13]).lower()==str(var_tran).lower()):# or str(row2[13]).lower()==var_tran or var2_tran==var_tran):
                    continue

                #print(variant)
                #flag = True
                #print(row_out1,"sorted_match production")
                # ws_out1.write(row_out1,0,row1[0])
                # ws_out1.write(row_out1,1,row1[1])
                # ws_out1.write(row_out1,2,row1[2])
                # ws_out1.write(row_out1,3,row1[3])
                # ws_out1.write(row_out1,4,row1[4])
                # ws_out1.write(row_out1,5,row1[5])
                # ws_out1.write(row_out1,6,row1[6])
                # ws_out1.write(row_out1,7,row1[7])
                # ws_out1.write(row_out1,8,row1[8])
                # ws_out1.write(row_out1,9,row1[9])
                # ws_out1.write(row_out1,10,row1[10])
                # ws_out1.write(row_out1,11,row1[11])
                # ws_out1.write(row_out1,12,row1[12])
                # ws_out1.write(row_out1,13,row1[13])
                # ws_out1.write(row_out1,14,row1[14])
                # ws_out1.write(row_out1,15,row1[15])
                
                ws_out1.write(row_out1,18,row2[0])
                ws_out1.write(row_out1,19,row2[1])
                ws_out1.write(row_out1,20,row2[2])
                ws_out1.write(row_out1,21,row2[3])
                ws_out1.write(row_out1,22,row2[4])
                ws_out1.write(row_out1,23,row2[5])
                ws_out1.write(row_out1,24,row2[6])
                ws_out1.write(row_out1,25,row2[7])
                ws_out1.write(row_out1,26,row2[8])
                ws_out1.write(row_out1,27,row2[9])
                ws_out1.write(row_out1,28,row2[10])
                ws_out1.write(row_out1,29,row2[11])
                ws_out1.write(row_out1,30,row2[12])
                ws_out1.write(row_out1,31,row2[13])
                ws_out1.write(row_out1,32,row2[14])
                ws_out1.write(row_out1,33,row2[15])
                ws_out1.write(row_out1,34,row2[16])
                ws_out1.write(row_out1,35,row2[17])
                ws_out1.write(row_out1,36,row2[18])
                ws_out1.write(row_out1,37,row2[19])
                ws_out1.write(row_out1,38,row2[20])
                ws_out1.write(row_out1,39,row2[21])
                ws_out1.write(row_out1,40,"sorted match production")
                is_entry_done = True
        
                flag = True
                #break

        for row_in2 in range(1,ws_in2.nrows):
            
            row2 = ws_in2.row_values(row_in2)
            
            start_year = 0 if (row2[6])=='' else int(row2[6])
            end_year = 1e9 if (row2[7])=='' else int(row2[7])
            
            if(reg_year<start_year):
                continue
            if((row2[5]).lower()==(var_variant).lower()):
                flag = True
                # ws_out1.write(row_out1,0,row1[0])
                # ws_out1.write(row_out1,1,row1[1])
                # ws_out1.write(row_out1,2,row1[2])
                # ws_out1.write(row_out1,3,row1[3])
                # ws_out1.write(row_out1,4,row1[4])
                # ws_out1.write(row_out1,5,row1[5])
                # ws_out1.write(row_out1,6,row1[6])
                # ws_out1.write(row_out1,7,row1[7])
                # ws_out1.write(row_out1,8,row1[8])
                # ws_out1.write(row_out1,9,row1[9])
                # ws_out1.write(row_out1,10,row1[10])
                # ws_out1.write(row_out1,11,row1[11])
                # ws_out1.write(row_out1,12,row1[12])
                # ws_out1.write(row_out1,13,row1[13])
                # ws_out1.write(row_out1,14,row1[14])
                # ws_out1.write(row_out1,15,row1[15])
                
                ws_out1.write(row_out1,18,row2[0])
                ws_out1.write(row_out1,19,row2[1])
                ws_out1.write(row_out1,20,row2[2])
                ws_out1.write(row_out1,21,row2[3])
                ws_out1.write(row_out1,22,row2[4])
                ws_out1.write(row_out1,23,row2[5])
                ws_out1.write(row_out1,24,row2[6])
                ws_out1.write(row_out1,25,row2[7])
                ws_out1.write(row_out1,26,row2[8])
                ws_out1.write(row_out1,27,row2[9])
                ws_out1.write(row_out1,28,row2[10])
                ws_out1.write(row_out1,29,row2[11])
                ws_out1.write(row_out1,30,row2[12])
                ws_out1.write(row_out1,31,row2[13])
                ws_out1.write(row_out1,32,row2[14])
                ws_out1.write(row_out1,33,row2[15])
                ws_out1.write(row_out1,34,row2[16])
                ws_out1.write(row_out1,35,row2[17])
                ws_out1.write(row_out1,36,row2[18])
                ws_out1.write(row_out1,37,row2[19])
                ws_out1.write(row_out1,38,row2[20])
                ws_out1.write(row_out1,39,row2[21])
                is_entry_done = True
        
                break

            variant2 = row2[5].lower().split(' ')
            var2_bt = ''
            var2_abs= ''
            var2_tran = ''
            var2_fuel = ''
            var2 = ''

            if(variant[0] not in variant2 or variant[1] not in variant2):
                continue
            
            if(flag):
                break
            
            for i in range(len(variant2)):
                if(variant2[i] in bt):
                    var2_bt = variant2[i]
                elif(variant2[i] in fuels):
                    var2_fuel = variant2[i]
                elif(variant2[i] in ABS):
                    var2_abs = variant2[i]
                elif(variant2[i] in transmission):
                    var2_tran = variant2[i]
                else:
                    var2+=(variant2[i])
                cnt=0
                if str(variant2[i]) not in variant:
                    cnt+=1
            sort2_var = ''.join(sorted(list(str(var2).replace(' ','').lower())))#.sort() #row1[0]
        
            if(sort_var==sort2_var):
                if not (str(row2[11]).lower()==str(var_fuel).lower()):# or str(row2[11]).lower()==var_fuel or var_fuel==var2_fuel):
                    continue
                #bt
                # if not (str(row2[12]).lower()==str(var_bt).lower()):# or str(row2[12]).lower()==var_bt or var_bt==var2_bt):
                #     continue
                #abs - Can't do as listings doesn't contain that
                #if not (str(row2[14]).lower()==str(row1[11]).lower() or str(row2[14]).lower()==var_fuel or var_fuel==var2_fuel):
                #    continue
                #tran
                if not (str(row2[13]).lower()==str(var_tran).lower()):# or str(row2[13]).lower()==var_tran or var2_tran==var_tran):
                    continue
                
                flag = True
                #print(row_out1,"sorted_match disc")
                
                # ws_out1.write(row_out1,0,row1[0])
                # ws_out1.write(row_out1,1,row1[1])
                # ws_out1.write(row_out1,2,row1[2])
                # ws_out1.write(row_out1,3,row1[3])
                # ws_out1.write(row_out1,4,row1[4])
                # ws_out1.write(row_out1,5,row1[5])
                # ws_out1.write(row_out1,6,row1[6])
                # ws_out1.write(row_out1,7,row1[7])
                # ws_out1.write(row_out1,8,row1[8])
                # ws_out1.write(row_out1,9,row1[9])
                # ws_out1.write(row_out1,10,row1[10])
                # ws_out1.write(row_out1,11,row1[11])
                # ws_out1.write(row_out1,12,row1[12])
                # ws_out1.write(row_out1,13,row1[13])
                # ws_out1.write(row_out1,14,row1[14])
                # ws_out1.write(row_out1,15,row1[15])
                
                ws_out1.write(row_out1,18,row2[0])
                ws_out1.write(row_out1,19,row2[1])
                ws_out1.write(row_out1,20,row2[2])
                ws_out1.write(row_out1,21,row2[3])
                ws_out1.write(row_out1,22,row2[4])
                ws_out1.write(row_out1,23,row2[5])
                ws_out1.write(row_out1,24,row2[6])
                ws_out1.write(row_out1,25,row2[7])
                ws_out1.write(row_out1,26,row2[8])
                ws_out1.write(row_out1,27,row2[9])
                ws_out1.write(row_out1,28,row2[10])
                ws_out1.write(row_out1,29,row2[11])
                ws_out1.write(row_out1,30,row2[12])
                ws_out1.write(row_out1,31,row2[13])
                ws_out1.write(row_out1,32,row2[14])
                ws_out1.write(row_out1,33,row2[15])
                ws_out1.write(row_out1,34,row2[16])
                ws_out1.write(row_out1,35,row2[17])
                ws_out1.write(row_out1,36,row2[18])
                ws_out1.write(row_out1,37,row2[19])
                ws_out1.write(row_out1,38,row2[20])
                ws_out1.write(row_out1,39,row2[21])
                ws_out1.write(row_out1,40,"sorted_match disc")
                is_entry_done = True
        
                

                

        for row_in3 in range(1,ws_in3.nrows):
            row2 = ws_in3.row_values(row_in3)
            if(flag):
                break
            start_year = 0 #if (row2[6])=='' else int(row2[6])
            end_year = 1e9 #if (row2[7])=='' else int(row2[7])
            if(reg_year<start_year):
                continue
            variant2 = row2[5].split(' ')
            var2_bt = ''
            var2_abs= ''
            var2_tran = ''
            var2_fuel = ''
            var2 = ''

            if(variant[0] not in variant2 or variant[1] not in variant2):
                continue
            
            for i in range(len(variant2)):
                if(variant2[i] in bt):
                    var2_bt = variant2[i]
                elif(variant2[i] in fuels):
                    var2_fuel = variant2[i] 
                elif(variant2[i] in ABS):
                    var2_abs = variant2[i]
                elif(variant2[i] in transmission):
                    var2_tran = variant2[i]
                else:
                    var2+=(variant2[i])
            cnt=0
            #temp = 3
            if(len(variant2) >= len(variant)):
                for i in range((len(variant2))):
                    if str(variant2[i]) not in variant:
                        cnt+=1#min(1,temp)
            else:
                for i in range((len(variant))):
                    if str(variant[i]) not in variant2:
                        cnt+=1#min(1,temp)
                    #temp-=1
                    #(len(variant)-(i+1))
                
            #sort2_var = ''.join(sorted(list(str(var2).replace(' ','').lower())))#.sort() #row1[0]
            var_majors = str(row2[3]).lower().split(' ')
            var_majors = var_majors[0]
            if(var_majors in variant):
                var_major = True
            else:
                var_major = False
            
            if(min_d>=cnt or var_major):

                #model insure
                if not(var_variant.startswith(row2[4])):
                    continue

                if(min_d_mv<1e9 and var_major==False):
                    continue
                
                #fuel
                if not (str(row2[11]).lower()==str(var_fuel).lower()):# or str(row2[11]).lower()==var_fuel or var_fuel==var2_fuel):
                    continue
                #bt
                # if not (str(row2[12]).lower()==str(var_bt).lower()):# or str(row2[12]).lower()==var_bt or var_bt==var2_bt):
                #     continue
                #abs - Can't do as listings doesn't contain that
                #if not (str(row2[14]).lower()==str(row1[11]).lower() or str(row2[14]).lower()==var_fuel or var_fuel==var2_fuel):
                #    continue
                #tran
                if not (str(row2[13]).lower()==str(var_tran).lower()):# or str(row2[13]).lower()==var_tran or var2_tran==var_tran):
                    continue

                # if(min_d==cnt and var_major==True):
                #     continue
                
                if not (var_major):
                    min_d = min(cnt,min_d)
                    #print(row_out1,min_d,"min-d update from prod")
                else:
                    if(min_d_mv<cnt):
                        continue
                    #print(row_out1,"Prod Var Major Matched with It's all Params")
                    min_d_mv = min(cnt,min_d_mv)

                #print(variant)
                #min_d = min(cnt,min_d)
                #print(row_out1,min_d,"min-d update from prod")
                # ws_out1.write(row_out1,0,row1[0])
                # ws_out1.write(row_out1,1,row1[1])
                # ws_out1.write(row_out1,2,row1[2])
                # ws_out1.write(row_out1,3,row1[3])
                # ws_out1.write(row_out1,4,row1[4])
                # ws_out1.write(row_out1,5,row1[5])
                # ws_out1.write(row_out1,6,row1[6])
                # ws_out1.write(row_out1,7,row1[7])
                # ws_out1.write(row_out1,8,row1[8])
                # ws_out1.write(row_out1,9,row1[9])
                # ws_out1.write(row_out1,10,row1[10])
                # ws_out1.write(row_out1,11,row1[11])
                # ws_out1.write(row_out1,12,row1[12])
                # ws_out1.write(row_out1,13,row1[13])
                # ws_out1.write(row_out1,14,row1[14])
                # ws_out1.write(row_out1,15,row1[15])
                
                ws_out1.write(row_out1,18,row2[0])
                ws_out1.write(row_out1,19,row2[1])
                ws_out1.write(row_out1,20,row2[2])
                ws_out1.write(row_out1,21,row2[3])
                ws_out1.write(row_out1,22,row2[4])
                ws_out1.write(row_out1,23,row2[5])
                ws_out1.write(row_out1,24,row2[6])
                ws_out1.write(row_out1,25,row2[7])
                ws_out1.write(row_out1,26,row2[8])
                ws_out1.write(row_out1,27,row2[9])
                ws_out1.write(row_out1,28,row2[10])
                ws_out1.write(row_out1,29,row2[11])
                ws_out1.write(row_out1,30,row2[12])
                ws_out1.write(row_out1,31,row2[13])
                ws_out1.write(row_out1,32,row2[14])
                ws_out1.write(row_out1,33,row2[15])
                ws_out1.write(row_out1,34,row2[16])
                ws_out1.write(row_out1,35,row2[17])
                ws_out1.write(row_out1,36,row2[18])
                ws_out1.write(row_out1,37,row2[19])
                ws_out1.write(row_out1,38,row2[20])
                ws_out1.write(row_out1,39,row2[21])
                if not (var_major):
                    ws_out1.write(row_out1,40,str(min_d)+" min-d update from prod")
                else:
                    ws_out1.write(row_out1,40,str(min_d_mv)+" Prod Var Major Match")
                
                is_entry_done = True

        for row_in2 in range(1,ws_in2.nrows):
            row2 = ws_in2.row_values(row_in2)
            if(flag):
                break
            start_year = 0 if (row2[6])=='' else int(row2[6])
            end_year = 1e9 if (row2[7])=='' else int(row2[7])
            
            if(reg_year<start_year):
                continue
            variant2 = row2[5].lower().split(' ')
            var2_bt = ''
            var2_abs= ''
            var2_tran = ''
            var2_fuel = ''
            var2 = ''

            if(variant[0] not in variant2 or variant[1] not in variant2):
                continue
            
            for i in range(len(variant2)):
                if(variant2[i] in bt):
                    var2_bt = variant2[i]
                elif(variant2[i] in fuels):
                    var2_fuel = variant2[i]
                elif(variant2[i] in ABS):
                    var2_abs = variant2[i]
                elif(variant2[i] in transmission):
                    var2_tran = variant2[i]
                else:
                    var2+=(variant2[i])
            cnt=0
            # for i in range(min(len(variant2),len(variant))):
            #     if str(variant2[i]) not in variant:
            #         cnt+=(len(variant2)-(i+1))
            #temp = 3
            if(len(variant2) >= len(variant)):
                for i in range((len(variant2))):
                    if str(variant2[i]) not in variant:
                        cnt+=1#min(1,temp)
            else:
                for i in range((len(variant))):
                    if str(variant[i]) not in variant2:
                        cnt+=1#min(1,temp)
                        #temp-=1
            # for i in range((len(variant))):
            #     if str(variant[i]) not in variant2:
            #         cnt+=1#(len(variant)-(i+1))
            var_majors = str(row2[3]).lower().split(' ')
            var_majors = var_majors[0]
            if(var_majors in variant):
                var_major = True
            else:
                var_major = False
                
            if(min_d>=cnt or (var_major)):
                #model insure
                if not(var_variant.startswith(row2[4])):
                    continue
                if(min_d_mv<1e9 and var_major==False):
                    continue
                #fuel
                if not (str(row2[11]).lower()==str(var_fuel).lower()):# or str(row2[11]).lower()==var_fuel or var_fuel==var2_fuel):
                    continue
                #bt
                # if not (str(row2[12]).lower()==str(var_bt).lower()):# or str(row2[12]).lower()==var_bt or var_bt==var2_bt):
                #     continue
                #abs - Can't do as listings doesn't contain that
                #if not (str(row2[14]).lower()==str(row1[11]).lower() or str(row2[14]).lower()==var_fuel or var_fuel==var2_fuel):
                #    continue
                #tran
                if not (str(row2[13]).lower()==str(var_tran).lower()):# or str(row2[13]).lower()==var_tran or var2_tran==var_tran):
                    continue

                if not (var_major):
                    min_d = min(cnt,min_d)
                    #print(row_out1,min_d,"min-d update from disc")
                else:
                    if(min_d_mv<cnt):
                        continue
                    #print(row_out1," Disc Var Major Matched with It's all Params")
                    min_d_mv = min(cnt,min_d_mv)


                #print(variant)
                # ws_out1.write(row_out1,0,row1[0])
                # ws_out1.write(row_out1,1,row1[1])
                # ws_out1.write(row_out1,2,row1[2])
                # ws_out1.write(row_out1,3,row1[3])
                # ws_out1.write(row_out1,4,row1[4])
                # ws_out1.write(row_out1,5,row1[5])
                # ws_out1.write(row_out1,6,row1[6])
                # ws_out1.write(row_out1,7,row1[7])
                # ws_out1.write(row_out1,8,row1[8])
                # ws_out1.write(row_out1,9,row1[9])
                # ws_out1.write(row_out1,10,row1[10])
                # ws_out1.write(row_out1,11,row1[11])
                # ws_out1.write(row_out1,12,row1[12])
                # ws_out1.write(row_out1,13,row1[13])
                # ws_out1.write(row_out1,14,row1[14])
                # ws_out1.write(row_out1,15,row1[15])
                

                ws_out1.write(row_out1,18,row2[0])
                ws_out1.write(row_out1,19,row2[1])
                ws_out1.write(row_out1,20,row2[2])
                ws_out1.write(row_out1,21,row2[3])
                ws_out1.write(row_out1,22,row2[4])
                ws_out1.write(row_out1,23,row2[5])
                ws_out1.write(row_out1,24,row2[6])
                ws_out1.write(row_out1,25,row2[7])
                ws_out1.write(row_out1,26,row2[8])
                ws_out1.write(row_out1,27,row2[9])
                ws_out1.write(row_out1,28,row2[10])
                ws_out1.write(row_out1,29,row2[11])
                ws_out1.write(row_out1,30,row2[12])
                ws_out1.write(row_out1,31,row2[13])
                ws_out1.write(row_out1,32,row2[14])
                ws_out1.write(row_out1,33,row2[15])
                ws_out1.write(row_out1,34,row2[16])
                ws_out1.write(row_out1,35,row2[17])
                ws_out1.write(row_out1,36,row2[18])
                ws_out1.write(row_out1,37,row2[19])
                ws_out1.write(row_out1,38,row2[20])
                ws_out1.write(row_out1,39,row2[21])
                if not (var_major):
                    ws_out1.write(row_out1,40,str(min_d)+" min-d update from disc")
                else:
                    ws_out1.write(row_out1,40,str(min_d_mv)+"Disc Var Major Match")
                    

                is_entry_done = True
        
               
        
        
                #break
        if(is_entry_done==False):
                # ws_out1.write(row_out1,0,row1[0])
                # ws_out1.write(row_out1,1,row1[1])
                # ws_out1.write(row_out1,2,row1[2])
                # ws_out1.write(row_out1,3,row1[3])
                # ws_out1.write(row_out1,4,row1[4])
                # ws_out1.write(row_out1,5,row1[5])
                # ws_out1.write(row_out1,6,row1[6])
                # ws_out1.write(row_out1,7,row1[7])
                # ws_out1.write(row_out1,8,row1[8])
                # ws_out1.write(row_out1,9,row1[9])
                # ws_out1.write(row_out1,10,row1[10])
                # ws_out1.write(row_out1,11,row1[11])
                # ws_out1.write(row_out1,12,row1[12])
                # ws_out1.write(row_out1,13,row1[13])
                # ws_out1.write(row_out1,14,row1[14])
                # ws_out1.write(row_out1,15,row1[15])
                #print("Nothing Similar Found Please Check for valid Params/variants with mastersheet manually")
                return 0
                
                

    wb_out.save('full_listing_specs.xls')


    if(is_entry_done):
        wb_in = xlrd.open_workbook(r'full_listing_specs.xls')
        sheet_name = wb_in.sheet_names()[0]
        ws_in1 = wb_in.sheet_by_name(sheet_name)

        for row_in1 in range(1,2):
            row = ws_in1.row_values(row_in1)
            # print(row[19])
            # print(row[20])
            # print(row[21])
            # print(row[22])
            # print(row[23])
            # print(row[24])
            # print(row[25])
            # print(row[26])
            # print(row[27])
            # print(row[28])
            # print(row[29])
            # print(row[30])
            # print(row[31])
            # print(row[32])
            # print(row[33])
            # print("Mumbai RTO : ",row[34])
            # print("Delhi RTO : ",row[35])
            # print("Banglore RTO : ",row[36])
            # print("Mumbai ORP : ", row[37])
            # print("Delhi ORP : ",row[38])
            # print("Banglore ORP : ",row[39])
            if city_id==1:
                return row[37]
            elif city_id==2:
                return row[38]
            else:
                return row[39]
    else:
        #print("Nothing Similar Found.")
        return 0
