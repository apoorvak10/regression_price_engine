#!/usr/bin/env python
# coding: utf-8

# In[3]:


import pandas as pd
import numpy as np
from numpy import inf
from IPython.display import display
from sklearn.neighbors import RadiusNeighborsRegressor
from scipy.sparse import csr_matrix
from sklearn.neighbors import KNeighborsRegressor
from sklearn import preprocessing
from sklearn.neighbors import NearestNeighbors
import math
import datetime
import statistics
import warnings
warnings.simplefilter('ignore')


# In[4]:


data_mumbai = pd.read_csv("KNN Dataset - Mumbai - Final.csv")
data_mumbai.replace("?", np.nan)
data_mumbai.rename(columns={'Variant ID': 'var_id'}, inplace=True)
data_mumbai.rename(columns={'ID': 'full_id'}, inplace=True)
data_mumbai.rename(columns={'Fuel Type': 'ftype'}, inplace=True)
data_mumbai.rename(columns={'Inventory Hold Up': 'inv_holdup'}, inplace=True)
data_mumbai['ftype'] = data_mumbai['ftype'].replace('Petrol',1)
data_mumbai['ftype'] = data_mumbai['ftype'].replace('Diesel',2)


data_delhi = pd.read_csv("KNN Dataset - Delhi - Final.csv")
data_delhi.replace("?", np.nan)
data_delhi.rename(columns={'Variant ID': 'var_id'}, inplace=True)
data_delhi.rename(columns={'ID': 'full_id'}, inplace=True)
data_delhi.rename(columns={'Fuel Type': 'ftype'}, inplace=True)
data_delhi.rename(columns={'Inventory Hold Up': 'inv_holdup'}, inplace=True)
data_delhi['ftype'] = data_delhi['ftype'].replace('Petrol',1)
data_delhi['ftype'] = data_delhi['ftype'].replace('Diesel',2)


data_bangalore = pd.read_csv("KNN Dataset - Bangalore - Final.csv")
data_bangalore.replace("?", np.nan)
data_bangalore.rename(columns={'Variant ID': 'var_id'}, inplace=True)
data_bangalore.rename(columns={'ID': 'full_id'}, inplace=True)
data_bangalore.rename(columns={'Fuel Type': 'ftype'}, inplace=True)
data_bangalore.rename(columns={'Inventory Hold Up': 'inv_holdup'}, inplace=True)
data_bangalore['ftype'] = data_bangalore['ftype'].replace('Petrol',1)
data_bangalore['ftype'] = data_bangalore['ftype'].replace('Diesel',2)
data_bangalore = data_bangalore[data_bangalore['ftype']!='Electric']


data_mumbai = data_mumbai.replace('N/A', float('NaN'))
data_delhi = data_delhi.replace('N/A', float('NaN'))
data_bangalore = data_bangalore.replace('N/A', float('NaN'))

data_mumbai = data_mumbai.dropna(subset = ['ASP'])
data_delhi = data_delhi.dropna(subset = ['ASP'])
data_bangalore = data_bangalore.dropna(subset = ['ASP'])

data_test = []

numeric_cols = ['full_id', 'var_id','Mileage', 'Model', 'Age', 'ASP', 'inv_holdup', 'ftype', 'Recency', 'ORP', 'Owners']

variables_variant = ['Owners','Mileage', 'Age','Recency', 'ASP']
features_variant = ['Owners', 'Mileage', 'Age', 'Recency']

variables_fuel = ['Owners','Mileage', 'Age', 'Recency', 'ORP', 'ASP', 'ftype']
features_fuel = ['Owners', 'Mileage', 'Age', 'Recency', 'ORP']

variables_model = ['Owners', 'Mileage', 'Age', 'Recency', 'ORP', 'ASP']
features_model = ['Owners', 'Mileage', 'Age', 'Recency', 'ORP']


data_mumbai['inv_holdup'] = data_mumbai['inv_holdup'].astype(int)
data_delhi['inv_holdup'] = data_delhi['inv_holdup'].astype(int)
data_bangalore['inv_holdup'] = data_bangalore['inv_holdup'].astype(int)

data_mumbai[numeric_cols] = data_mumbai[numeric_cols].astype(float)
data_delhi[numeric_cols] = data_delhi[numeric_cols].astype(float)
data_bangalore[numeric_cols] = data_bangalore[numeric_cols].astype(float)


data_mumbai = data_mumbai[data_mumbai['inv_holdup'] < 45]
data_delhi = data_delhi[data_delhi['inv_holdup'] < 45]
data_bangalore = data_bangalore[data_bangalore['inv_holdup'] < 45]


# In[5]:


def knn(va_id, ful_id, fuel_id, min_neighbors, max_radius, city_id):
    
    age_weight = 5
    recency_weight = 0.5
    orp_weight = 2
    owners_weight = 1
    mileage_weight = 0.5
    
    
    if(city_id==1):
        da_train = data_mumbai
        da_train = da_train[da_train.full_id != ful_id]
    elif(city_id==2):
        da_train = data_delhi
        da_train = da_train[da_train.full_id != ful_id]
    elif(city_id==5):
        da_train = data_bangalore
        da_train = da_train[da_train.full_id != ful_id]
    
    
    if(va_id!=None and ful_id!=None and fuel_id==None):
        
        df_train = da_train[variables_variant].loc[da_train['var_id'] == va_id]
        
        df_test = data_test[variables_variant]
        
        if(len(df_train)<min_neighbors):
            fuel_type = data_test['ftype'].iloc[0]
            return knn(va_id, ful_id, fuel_type, min_neighbors, max_radius, city_id)
        
        feat = features_variant
                
    elif(va_id!=None and ful_id!=None and fuel_id!=None):
        
        model = data_test['Model'].iloc[0]
        
        df_train = da_train[variables_fuel].loc[da_train['Model'] == model]
        df_train = df_train.loc[df_train['ftype'] == fuel_id]
        df_train = df_train.drop(['ftype'],axis=1)
        
        df_train = df_train.dropna(subset = ['ORP'])
        
        df_test = data_test[variables_fuel]
        df_test = df_test.drop(['ftype'],axis=1)
        
        df_test = df_test.dropna(subset = ['ORP'])
        
        if(len(df_test)==0):
            return None
        
        if(len(df_train)<min_neighbors):
            return knn(None, ful_id, None, min_neighbors, max_radius, city_id)
            
        feat = features_fuel
            
    elif(va_id==None and ful_id!=None and fuel_id==None):
        
        model = data_test['Model'].iloc[0]
    
        df_train = da_train[variables_model].loc[da_train['Model']==model]
        df_train = df_train.dropna(subset = ['ORP'])
    
        df_test = data_test[variables_model]
        df_test = df_test.dropna(subset = ['ORP'])
        
        feat = features_model
        
        if(len(df_test)==0):
            return None
     
        if(len(df_train)<min_neighbors):
            return None

    
    df_train['Recency'] = df_train['Recency'] - df_test['Recency'].iloc[0]
    df_test['Recency'] = 0
    
    df_train['Recency'] = np.where(df_train['Recency']%30==0, df_train['Recency']/30, df_train['Recency']//30+1)
    
    normalized_train = (df_train-df_train.min())/(df_train.max()-df_train.min())
    normalized_train['ASP'] = df_train['ASP']

    normalized_test = (df_test-df_train.min())/(df_train.max()-df_train.min())
    normalized_test['ASP'] = df_test['ASP']
    
    normalized_train['Age'] = normalized_train['Age']*age_weight
    normalized_test['Age'] = normalized_test['Age']*age_weight
    
    normalized_train['Recency'] = normalized_train['Recency']*recency_weight
    normalized_test['Recency'] = normalized_test['Recency']*recency_weight
    
    normalized_train['Mileage'] = normalized_train['Mileage']*mileage_weight
    normalized_test['Mileage'] = normalized_test['Mileage']*mileage_weight
    
    
    if not(va_id!=None and ful_id!=None and fuel_id==None):
        normalized_train['ORP'] = normalized_train['ORP']*orp_weight
        normalized_test['ORP'] = normalized_test['ORP']*orp_weight
    
    normalized_train['Owners'] = normalized_train['Owners']*owners_weight
    normalized_test['Owners'] = normalized_test['Owners']*owners_weight
    
    
    normalized_train = normalized_train.fillna(1)
    normalized_train = normalized_train.replace([inf, -inf], 1)
    
    normalized_test = normalized_test.fillna(1)
    normalized_test = normalized_test.replace([inf, -inf], 1)
    
    
    
    neigh = NearestNeighbors(radius=max_radius)
    neigh.fit(normalized_train[feat])
    rng = neigh.radius_neighbors_graph(normalized_test[feat].iloc[[0]])
    
    rng1 = neigh.radius_neighbors(normalized_test[feat].iloc[[0]])
        
    neighbor = rng[0][0].nnz
    
    if(rng[0][0].nnz < min_neighbors):
        if(va_id!=None and ful_id!=None and fuel_id==None):
            fuel_type = data_test['ftype'].iloc[0]
            return knn(va_id, ful_id, fuel_type, min_neighbors, max_radius, city_id)
                    
        elif(va_id!=None and ful_id!=None and fuel_id!=None):
            return knn(None, ful_id, None, min_neighbors, max_radius, city_id)
                        
        elif(va_id==None and ful_id!=None and fuel_id==None):
            return None
            
    else:
        knn1 = RadiusNeighborsRegressor(radius = max_radius, weights='distance', p=2)
        knn1.fit(normalized_train[feat], normalized_train['ASP'])
        
        prediction = float(knn1.predict(normalized_test[feat])[0])
        return prediction


# In[6]:


def compute(listing_id, variant_id, model, mileage, age, asp, inv_holdup, fuel_type, recency, orp, number_of_owners , minimum_neighbors, maximum_radius, city_id):
    
    global data_test
    
    input_data = [listing_id, variant_id, model, mileage, age, asp, inv_holdup, fuel_type, recency, orp, number_of_owners, city_id]
    
    data_test = pd.DataFrame([input_data], columns = ['full_id', 'var_id', 'Model', 'Mileage', 'Age', 'ASP', 'inv_holdup', 'ftype', 'Recency', 'ORP', 'Owners', 'city_id'])
    data_test = data_test.replace('N/A',float('NaN'))
    data_test = data_test.replace('N/A',float('NaN'))
    data_test['inv_holdup'] = data_test['inv_holdup'].astype(int)
    data_test['ftype'] = data_test['ftype'].replace('Petrol',1)
    data_test['ftype'] = data_test['ftype'].replace('Diesel',2)
    
    data_test[numeric_cols] = data_test[numeric_cols].astype(float)
    
    if(data_test['inv_holdup'].iloc[0] >= 45):
        output = None
    else: 
        output = knn(variant_id, listing_id, None, minimum_neighbors, maximum_radius, city_id)
    
    return output


# In[9]:


#compute(32019, 1672, 306, 1.1879, 6, 250000, 2, 'Diesel', 436, 540140, 1, 2, 1, 5)


# In[10]:


#compute(47212,1100,471,6.0907,6,1,20,'Petrol', 192,520000,1,2,5,1)

