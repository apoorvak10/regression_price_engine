import xlrd
import xlwt
import difflib 
import re
def closeMatches(patterns, word): 
     print(get_close_matches(word, patterns))


wb_in1 = xlrd.open_workbook(r'listing_orps_new.xls')
sheet_name = wb_in1.sheet_names()[0]
ws_in1 = wb_in1.sheet_by_name(sheet_name)



def get_orp(listing_id,city_id):
    for row_in1 in range(ws_in1.nrows):
        row1 = ws_in1.row_values(row_in1)
        #print(row1[37])
        if row1[4] is '':
            continue
        if(int(row1[4])==int(listing_id)):
            if(city_id==1):
                return str(row1[37])
            elif(city_id==2):
                return str(row1[38])
            else:
                return str(row1[39])
        else:
            continue

print("Looking for ORP ?")
listing_id = int(input("enter listing id :"))
city_id = int(input("enter_city_id (1-Mumbai,2-Delhi,5-Banglore):"))

print(get_orp(listing_id,city_id))